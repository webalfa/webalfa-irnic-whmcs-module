<?php

$additionaldomainfields[".ir"][] = array("Name" => "holder_handle", "LangVar" => "WEBALFA_IRNIC_HANDLE","Type" => "text","Size" => "30","Default" => "","Required" => true);

$additionaldomainfields[".co.ir"] = $additionaldomainfields[".ir"];
$additionaldomainfields[".ac.ir"] = $additionaldomainfields[".ir"];
$additionaldomainfields[".net.ir"] = $additionaldomainfields[".ir"];
$additionaldomainfields[".org.ir"] = $additionaldomainfields[".ir"];
$additionaldomainfields[".id.ir"] = $additionaldomainfields[".ir"];
$additionaldomainfields[".sch.ir"] = $additionaldomainfields[".ir"];
$additionaldomainfields[".gov.ir"] = $additionaldomainfields[".ir"];